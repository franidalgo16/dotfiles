#!/usr/bin/env bash

confirm="$HOME/.config/qtile/custom/rofi/confirm.sh"
theme="-theme $HOME/.config/qtile/custom/rofi/themes/powermenu.rasi"


#Options

uptime=$(uptime -p)

shutdown="襤"
restart="ﰇ"

options="$restart\n$shutdown"

chosen="$(echo -e "$options" | rofi $theme -p "$uptime" -dmenu -selected-row 2)"
case $chosen in
$shutdown)
    exec $confirm "systemctl poweroff" &
    ;;
$restart)
    exec $confirm "systemctl reboot" &
    ;;
esac
