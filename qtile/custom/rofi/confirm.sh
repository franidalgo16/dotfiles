#!/usr/bin/env bash

msg="Options : yes / y / no / n"
r_theme="$HOME/.config/rofi/confirm.rasi"

ans="$(rofi -dmenu -i -no-fixed-num-lines -p "Are you sure?" -theme "$r_theme")"
if [[ $ans == "yes" ]] || [[ $ans == "YES" ]] || [[ $ans  = "y" ]] || [[ $ans == "Y" ]]
then
    $1
elif [[ $ans == "no" ]] || [[ $ans == "NO" ]] || [[ $ans == "n" ]] || [[ $ans == "N" ]]
then
    exit
else
    rofi -theme "$r_theme" -e "$msg"
fi
