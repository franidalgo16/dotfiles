import os
import subprocess

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class MyWindow(Gtk.Window):
    
    def __init__(self):
        super().__init__(title="Hola")
        self.button = Gtk.Button(label = "Hace click gato")
        self.button.connect("clicked", self.on_button_clicked)
        self.add(self.button)

    def on_button_clicked(self, wdiget):
        print("Bien ahi")



if __name__ ==  '__main__':
    win = MyWindow()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()
