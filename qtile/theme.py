class Theme:

    def __init__(self, normal, focus, upper_bar='#000000', lower_bar='#111111', upper_bar_op=0.8, logout="#650aff", reboot="#009688", shutdown="#96000e", active="FFFFFF", inactive="404040", group_hl=None):
        self.normal_color = normal
        self.focus_color = focus
        self.upper_bar_color = upper_bar
        self.lower_bar_color = lower_bar
        self.upper_bar_op = upper_bar_op
        self.logout_color = logout
        self.reboot_color = reboot
        self.shutdown_color = shutdown
        self.active_color = active
        self.inactive_color = inactive
        if group_hl is None:
            self.group_hl = focus
        else:
            self.group_hl = group_hl