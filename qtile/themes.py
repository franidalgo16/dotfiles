from theme import Theme

default = Theme(normal="#061d1b", focus="009688")

nord = Theme(
    normal="#434C5E", focus="#88C0D0",
    upper_bar="#2E3440", lower_bar="#3B4252", upper_bar_op=1,
    logout="#B48EAD", reboot="#A3BE8C", shutdown="#BF616A",
    active="ECEFF4", inactive="4C566A",
    group_hl="#5E81AC")

tv = Theme(
    normal="#FFF4E4", focus="#6EFAFB",
    upper_bar="#37277C", lower_bar="#FF57BB", upper_bar_op=1,
    shutdown="#E74C3C", reboot="#00FF7F", logout="#F7CA18",
    active="FF57BB", inactive="5a4d96",
    group_hl="#F7E8A4")
