# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import subprocess
from collections import namedtuple
from typing import List  # noqa: F401

from libqtile import bar, hook, layout, qtile, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

from my_widgets import QuickReboot, QuickShutdown
from themes import *

mod = "mod4"
terminal = "kitty"
browser = "brave"

#Themes
theme = nord

# Custom functions
def alt_mod():
    if mod == "mod4":
        return "mod1"
    else:
        return "mod4"

def power_menu(qtile):
    script_path = os.path.expanduser("~/.config/qtile/custom/rofi/power_menu.sh")
    subprocess.call([script_path])

Color_set  = namedtuple('Color_set', 'normal focus')
my_colors= Color_set('#061d1b', '#009688')

keys = [
    # Multimedia
    Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioStop", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pavolume volup --quiet")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pavolume voldown --quiet")),
    Key([], "XF86AudioMute", lazy.spawn("pavolume mutetoggle --quiet")),
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl set 10%+")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl set 10%-")),
    # Custom ones babe
    Key([alt_mod()], "Tab", lazy.spawn('rofi -show window'), desc="Launches rofi window manager"),
    Key([mod], "e", lazy.spawn('rofi -show drun'), desc="Launches rofi program list"),
    Key([mod], "f", lazy.spawn('pcmanfm')),
    Key([mod, "shift"], "f", lazy.spawn('rofi -show filebrowser'), desc="Launches rofi file browser"),
    Key([mod], "b", lazy.spawn(browser)),
    Key([mod], "p", lazy.function(power_menu)),
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.swap_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.swap_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.swap_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.swap_up(), desc="Move window up"),

    # For Column
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    # Key([mod, "control"], "h", lazy.layout.grow_left(),
    #    desc="Grow window to the left"),
    # Key([mod, "control"], "l", lazy.layout.grow_right(),
    #    desc="Grow window to the right"),
    # Key([mod, "control"], "j", lazy.layout.grow_down(),
    #    desc="Grow window down"),
    # Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),

    # For MonadTall
    Key([mod], "i", lazy.layout.grow()),
    Key([mod], "m", lazy.layout.shrink()),
    Key([mod], "o", lazy.layout.maximize()),
    Key([mod, "shift"], "space", lazy.layout.flip()),

    # Generic
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),
]

groups = [
    Group(name, **kwargs)
    for name, kwargs in [
            ("WEB", {"matches": [Match(wm_class=["brave-browser"])]}),
            ("DEV1", {}),
            ("DEV2", {}),
            ("MISC", {}),
            ("DISC", {"matches": [Match(wm_class=["discord"])]}),
            ("MUS", {"matches": [Match(wm_class=["spotify"])]}),
            ("FM", {"matches": [Match(wm_class=["pcmanfm"])]}),
            ("DOOM",{"matches": [Match(wm_class=["emacs"])], "persist":False, "init":False, "layout":'max'})
    ]
]

for i, group in enumerate(groups, 1):
    keys.extend(
        [
            Key(
                [mod], str(i), 
                lazy.group[group.name].toscreen(toggle=False),
                desc="Switch to group {group.name}"
            ),
            Key(
                [mod, "shift"], str(i),
                lazy.window.togroup(group.name),
                desc="Move focused window to group {group.name}"
            )
        ]
    )

my_layout_config = {
    "border_focus" : theme.focus_color,
    "border_focus_stack": theme.focus_color,
    "border_normal": theme.normal_color,
    "border_normal_stack":theme.normal_color,
    "margin": 4
}

layouts = [
    # layout.Bsp(**my_layout_config),
    layout.MonadTall(**my_layout_config),
    #layout.Columns(**my_layout_config),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='FiraCode Nerd Font',
    fontsize=12,
    padding=4
)
extension_defaults = widget_defaults.copy()


# Mouse Callbacks

screens = [
    Screen(
        top=bar.Bar(
            [   
                widget.GroupBox(
                    this_current_screen_border=theme.group_hl,
                    borderwidth=2,
                    disable_drag=True,
                    highlight_method='block',
                    rounded=False,
                    active=theme.active_color,
                    inactive=theme.inactive_color
                ),
                widget.Prompt(),
                widget.WindowName(),
                widget.CurrentLayout(),
                widget.CurrentLayoutIcon(custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")]),
                widget.Sep(),
                widget.Clock(format='%Y-%m-%d  %a  %I:%M %p'),
                widget.Sep(),
                # widget.PulseVolume(fmt='Vol {}'),
                widget.Systray(),
                # widget.Battery(
                #    format='  {percent:2.0%} {char}',
                #    charge_char='',
                #    discharge_char='',
                #    empty_char='',
                #    notify_below=30
                # ),
                widget.QuickExit(
                    foreground=theme.logout_color,
                    padding=8,
                    fontsize=20,
                    default_text='﫼',
                    countdown_format='{}'
                ),
                QuickReboot(
                    foreground=theme.reboot_color,
                    padding=8,
                    fontsize=20,
                    default_text='ﰇ',
                    countdown_format='{}'
                ),
                QuickShutdown(
                    foreground=theme.shutdown_color,
                    padding=8,
                    fontsize=20,
                    default_text='',
                    countdown_format='{}'
                )
            ],
            30,
            background=theme.upper_bar_color, 
            opacity=theme.upper_bar_op,
        ),
        bottom=bar.Bar(
            [
                widget.CheckUpdates(
                    distro="Arch_paru", 
                    display_format="There are {updates} updates!",
                    no_update_string = "No updates"
                ),
                widget.Spacer(length = bar.STRETCH),
                #widget.Wlan(
                #    interface='wlp6s0',
                #    format='{essid}'
                # ),
                widget.Net(
                    interface='enp6s0',
                    format='NET {down}  {up} ',
                ),
                widget.Sep(),
                widget.NvidiaSensors(format='GPU {temp}°C {perf}'),
                widget.Sep(),
                widget.CPU(),
                widget.Sep(),
                widget.Memory(fmt="Mem {}")
            ],
            size=17,
            background=theme.lower_bar_color
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(
    border_focus=theme.focus_color,
    border_normal=theme.normal_color,
    float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(title="shutdown_confirmation"),
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(func=lambda c: bool(c.is_transient_for())),
    Match(wm_class="pavucontrol"),
    Match(wm_class="lxappearance")
])

auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# Startup function
@hook.subscribe.startup_once
def autostart():
    process_path = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([process_path])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
